import pandas as pd
import numpy as np
from sklearn import linear_model


def Logistic(X_train, y_train, X_test, y_test):

    clf = linear_model.LogisticRegression()
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    accuracy = clf.score(X_test, y_test)
    probs = clf.predict_proba(X_test)
    coefs = clf.coef_
    intercept = clf.intercept_

    return y_pred, accuracy, probs, coefs, intercept


def yearly_iterator(df_Final, given_year):
    Sliced = df_Final[df_Final['year'] < given_year]
    y_train = Sliced['defl']
    X_train = Sliced.drop(['defl', 'year'], axis=1)

    Sliced_t = df_Final[df_Final['year'] == given_year]
    y_test = Sliced_t['defl']
    X_test = Sliced_t.drop(['defl', 'year'], axis=1)

    Y_pred, accuracy, probs, coefs1, intercept1 = Logistic(X_train, y_train, X_test, y_test)
    print 'Classification Accuracy (%d data): %f' % (given_year, accuracy)

    temp = X_test.multiply(coefs1[0], axis=1)
    # Q = exp( intercept + (Factors * coefficients) )
    Q = np.exp(temp.sum(axis=1) + intercept1)
    P = Q / (1 + Q)

    Iteration_df = pd.DataFrame(columns=['year', 'defl', 'P'], index=None)

    Iteration_df['defl'] = y_test
    Iteration_df['P'] = P
    Iteration_df.fillna(value=given_year, inplace=True)
    return Iteration_df


if __name__ == '__main__':
    # Reading all LoanStats Datasets
    print 'Reading Datasets...'
    required_cols = ['loan_status', 'loan_amnt', 'annual_inc', 'installment', 'funded_amnt', 'total_pymnt', 'revol_bal', 'total_rec_late_fee', 'issue_d', 'dti']
    df_a = pd.read_csv('LoanStats3a.csv', usecols=required_cols, header=0)
    df_b = pd.read_csv('LoanStats3b.csv', usecols=required_cols, header=0)
    df_c = pd.read_csv('LoanStats3c.csv', usecols=required_cols, header=0)
    df_d = pd.read_csv('LoanStats3d.csv', usecols=required_cols, header=0)
    df_161 = pd.read_csv('LoanStats_2016Q1.csv', usecols=required_cols, header=0)
    df_162 = pd.read_csv('LoanStats_2016Q2.csv', usecols=required_cols, header=0)

    # Combining all all datasets
    df_LoanStats = df_a.append(df_b)
    df_LoanStats = df_LoanStats.append(df_c)
    df_LoanStats = df_LoanStats.append(df_d)
    df_LoanStats = df_LoanStats.append(df_161)
    df_LoanStats = df_LoanStats.append(df_162)

    # Filtering data and creating new variables
    df_LoanStats = df_LoanStats[(df_LoanStats.loan_status == 'Default') | (df_LoanStats.loan_status == 'Charged Off') | (df_LoanStats.loan_status == 'Fully Paid')]
    df_LoanStats['defl'] = [1 if (x == 'Default') | (x == 'Charged Off') else 0 for x in df_LoanStats['loan_status']]
    df_LoanStats['amt_ratio'] = df_LoanStats['loan_amnt'] / df_LoanStats['annual_inc']
    df_LoanStats['installment_ratio'] = df_LoanStats['installment'] / df_LoanStats['annual_inc']
    df_LoanStats['funded_amt_ratio'] = df_LoanStats['funded_amnt'] / df_LoanStats['annual_inc']
    df_LoanStats['total_pymnt_ratio'] = df_LoanStats['total_pymnt'] / df_LoanStats['annual_inc']
    df_LoanStats['revol_bal_ratio'] = df_LoanStats['revol_bal'] / df_LoanStats['annual_inc']
    df_LoanStats['late_fee_ratio'] = df_LoanStats['total_rec_late_fee'] / df_LoanStats['loan_amnt']
    df_LoanStats['year'] = 2000 + (df_LoanStats['issue_d'].str[0:-4]).astype(int)

    # Keeping necessary columns
    df_Final = pd.concat([df_LoanStats['defl'], df_LoanStats['year'], df_LoanStats['amt_ratio'], df_LoanStats['installment_ratio'],
                          df_LoanStats['funded_amt_ratio'], df_LoanStats['total_pymnt_ratio'], df_LoanStats['revol_bal_ratio'],
                          df_LoanStats['late_fee_ratio'], df_LoanStats['dti']], axis=1,
                         keys=['defl', 'year', 'amt_ratio', 'installment_ratio', 'funded_amt_ratio', 'total_pymnt_ratio', 'revol_bal_ratio', 'late_fee_ratio', 'dti'])

    # Sorting filtered data by year
    df_Final = df_Final.sort_values(by='year', ascending=[True])

    print 'In-Sample Estimation Start!'
    # Remove outliers before classification
    df_Final = df_Final.replace(np.inf, np.nan)
    df_Final.dropna(axis=0, how='any', inplace=True)

    # Setting up Training/Testing Data
    Sliced = df_Final[df_Final['year'] < 2017]
    y_train = Sliced['defl']
    X_train = Sliced.drop(['defl', 'year'], axis=1)

    Y_pred, accuracy, probs, coefs, intercept = Logistic(X_train, y_train, X_train, y_train)

    print '-------------- Logistic Regression Coefficients ----------------'
    stats_df = pd.DataFrame(coefs, columns=X_train.columns.values)
    print stats_df
    print 'Intercept value: ', intercept
    print '----------------------------------------------------------------'

    print 'In-Sample Accuracy: %f' % accuracy
    print

    print 'Out of Sample Estimation Start!'
    print
    Iteration_df = yearly_iterator(df_Final, 2015)
    Iteration_df2 = yearly_iterator(df_Final, 2016)

    All_iterations = pd.concat([Iteration_df, Iteration_df2], ignore_index=True)
    print
    print 'Out of Sample Estimation End!'
    print

    # Get Deciles
    print 'Deciles by frequency'
    All_iterations = All_iterations.sort_values(by='P', ascending=[False])
    All_iterations['decile'] = pd.qcut(All_iterations['P'], 10, labels=False)

    abc = All_iterations.groupby(by=['decile'])['defl'].sum()

    deciles_df = abc.to_frame(name='freq')
    deciles_df = deciles_df.sort_values(by='freq', ascending=[False])
    deciles_df['cum_sum'] = deciles_df['freq'].cumsum()
    deciles_df['cum_perc'] = 100 * deciles_df.cum_sum / deciles_df.freq.sum()
    print deciles_df
