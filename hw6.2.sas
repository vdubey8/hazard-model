/*	Name: Varun Dubey
	GTID: 903225433
	Assignment 6.2
*/

libname storage "Q:\Scratch\vd8\6\2";

/* MACRO - SORT DATA */
%macro sort_table(param1, param2);
	proc sort data = &param1;
		by &param2;
	run;
%mend;

/* MACRO - CALL LOGISTIC ITERATOR */
%macro start_iterator;

	%do year = 2015 %to 2016;
     	%logistic_iterator(&year);
	%end;

%mend;

%macro logistic_iterator(given_year);

	/* SAMPLE DATA TILL GIVEN_YEAR VALUE */
	data storage.sample_input;
		set storage.LoansData;
		if YEAR < &given_year;
	run;

	proc logistic data=storage.sample_input outest=storage.sample_output descending noprint;
		model defl = total_acc late_fee_ratio amnt_ratio funded_amnt_ratio installment_ratio total_pymnt_ratio revol_bal_ratio dti;
	run;
	quit;

	/* SET FACTOR COEFFICIENTS */
	data storage.sample_output;
		set storage.sample_output;

		year = &given_year;

		total_acc_beta = total_acc;
		late_fee_ratio_beta = late_fee_ratio;
		amnt_ratio_beta = amnt_ratio;
		funded_amnt_ratio_beta = funded_amnt_ratio;
		installment_ratio_beta = installment_ratio;
		total_pymnt_ratio_beta = total_pymnt_ratio;
		revol_bal_beta = revol_bal_ratio;
		dti_beta = dti;

		keep year intercept total_acc_beta late_fee_ratio_beta amnt_ratio_beta funded_amnt_ratio_beta installment_ratio_beta total_pymnt_ratio_beta revol_bal_beta dti_beta;
	run;

	data storage.yearly_sample;
		set storage.LoansData;
		if year = &given_year;
	run;

	/* COMPUTE PROBABILITY FOR EACH FACTOR */
	data storage.sample_output;
		merge storage.sample_output storage.yearly_sample;
		by YEAR;

		Q = exp(total_acc_beta*total_acc +
				late_fee_ratio_beta*late_fee_ratio +
				amnt_ratio_beta*amnt_ratio +
				funded_amnt_ratio_beta*funded_amnt_ratio +
				installment_ratio_beta*installment_ratio +
				total_pymnt_ratio_beta*total_pymnt_ratio +
				revol_bal_beta*revol_bal_ratio +
				dti_beta*dti +
				intercept);

		P = Q/(1+Q); 
		if P=. then delete;

		keep year defl P;
	run;

	/* APPEND ITERATION DATA TO FULL DATASET CONTAINING ALL ITERATIONS */
	data storage.iterator_data;
		merge storage.iterator_data storage.sample_output;
		by YEAR;

		if YEAR=0 then delete;
	run;

%mend;

/* IMPORT ALL LOANSTATS DATESETS */

	proc import datafile="P:\MGMT of Fin\A6\LoanStats3a.csv" replace
		out=LoanStats3a dbms=csv;
		getnames=yes;
	run;

	proc import datafile="P:\MGMT of Fin\A6\LoanStats3b.csv"
		out=LoanStats3b dbms=csv;
		getnames=yes;
	run;

	proc import datafile="P:\MGMT of Fin\A6\LoanStats3c.csv"
		out=LoanStats3c dbms=csv;
		getnames=yes;
	run;

	proc import datafile="P:\MGMT of Fin\A6\LoanStats3d.csv"
		out=LoanStats3d dbms=csv;
		getnames=yes;
	run;

	proc import datafile="P:\MGMT of Fin\A6\LoanStats_2016Q1.csv"
		out=LoanStats2016Q1 dbms=csv;
		getnames=yes;
	run;

	proc import datafile="P:\MGMT of Fin\A6\LoanStats_2016Q2.csv"
		out=LoanStats2016Q2 dbms=csv;
		getnames=yes;
	run;

/* COMBINE ALL LOANSTATS DATASETS AND ADD DEFAULT COLUMN */
	data storage.LoansData;
		set LoanStats3a LoanStats3b LoanStats3c LoanStats3d LoanStats2016Q1 LoanStats2016Q2;

		if loan_status="Default" or loan_status="Charged Off" then defl=1;
		else if loan_status="Fully Paid" then defl=0;
		else delete;
		
	/* ADD EXPLANATORY VARIABLES */
		amnt_ratio = loan_amnt/annual_inc;
		funded_amnt_ratio = funded_amnt/annual_inc;
		installment_ratio = installment/annual_inc;
		total_pymnt_ratio = total_pymnt/annual_inc;
		revol_bal_ratio = revol_bal/annual_inc;
		late_fee_ratio = total_rec_late_fee/loan_amnt;
	/* GET CORRECT YEAR */	
		fyear = substr(issue_d,1,(length(issue_d)-4));
		if length(fyear)=1 then fyear = ("200" || fyear);
		else fyear = ("20" || fyear);
		year = input(fyear,4.0);

		keep year defl total_acc late_fee_ratio amnt_ratio funded_amnt_ratio installment_ratio total_pymnt_ratio revol_bal_ratio dti;
	run;

ods graphics on;
ods pdf file = "P:\MGMT of Fin\A6\Assignment_6_2.pdf";

	%sort_table(storage.LoansData, year);

/* IN SAMPLE ESTIMATION */

/* TRAINING THE REGRESSION MODEL */
	proc logistic data=storage.LoansData outest=insample_output plots(only)=(roc effect) descending;
		title "IN SAMPLE ESTIMATION";
		model defl = total_acc late_fee_ratio amnt_ratio funded_amnt_ratio installment_ratio total_pymnt_ratio revol_bal_ratio dti;
	run;

/* OUT OF SAMPLE ESTIMATION */

/* INITIALIZE BLANK DATASET TO APPEND REGRESSION VALUES */
	data storage.iterator_data;
		YEAR = 0;
	run;

	%start_iterator;

/* SORTING DECILES BY PROBABILITY */
	%sort_table(storage.iterator_data, P);
	proc rank data=storage.iterator_data groups=10 out=storage.decile_out;
		var P;
		ranks P_rank;
	run;

	%sort_table(storage.decile_out, P_rank);

/* GETTING CUMULATIVE DATA */
	proc means data=storage.decile_out sum noprint;
		var defl;
		output out=storage.decile_distribution sum=;
		by P_rank;
	run;

/* RANKING DECILES BY THE NUMBER OF DEFAULTS */
	proc freq data=storage.decile_out order=freq;
		title "RANKING OUTPUT";
		weight defl;
		tables P_Rank;
	run;

ods pdf close;
